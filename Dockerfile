FROM ubuntu
ENV DEBIAN_FRONTEND=noninteractive
RUN apt update && \
    apt install qemu-kvm *zenhei* xz-utils dbus-x11 curl firefox gnome-system-monitor mate-system-monitor git xfce4 xfce4-terminal tightvncserver wget websockify -y && \
    apt install git curl wget repo ccache gcc g++ gcc-multilib g++-multilib ccache ninja-build python3 python3-pip python-is-python3 vim -y && \
    apt install openssh-server net-tools unzip tar sudo -y && \
    apt install -y --no-install-recommends sqlite3 && \
    apt install docker lsb-release -y

RUN rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app
COPY requirements.txt ./
RUN pip install -r requirements.txt
COPY . .

EXPOSE 8000 
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
